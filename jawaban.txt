Jawaban

1. Buat Database

create database myshop;

2. buat tabel

CREATE TABLE users( 
id int PRIMARY KEY AUTO_INCREMENT, 
name varchar(255), 
email varchar(255), 
password varchar(255) 
);

CREATE TABLE categories( 
id int AUTO_INCREMENT PRIMARY KEY,
name varchar(255) 
);

CREATE TABLE items( 
    id int AUTO_INCREMENT PRIMARY KEY, 
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    FOREIGN KEY(category_id) REFERENCES categories(id)
);


3. Masukkan data dalam tabel

INSERT INTO users (name, email, password) 
VALUES ("John Doe", "john@doe.com", "john123"), 
("Jane Doe", "jane@doe.com", "jenita");

INSERT INTO categories(name) VALUES 
("gadget"), 
("cloth"), 
("men"), 
("woman"), 
("branded");


INSERT INTO items(name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", "4000000", 100, 1);
INSERT INTO items(name, description, price, stock, category_id) VALUES ("Uniklooh", "baju keren dari brand ternama", "500000", 50, 2);
INSERT INTO items(name, description, price, stock, category_id) VALUES ("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", 10, 1);

4. Ambil data dari database

a. ambil data users
SELECT id, name, email FROM users;

b. ambil data items
SELECT * FROM items WHERE price > 1000000;

SELECT * FROM items WHERE name LIKE "uniklooh";

c.Menampilkan data items join dengan categories
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori FROM items INNER JOIN categories ON items.category_id = categories.id;

5. Ubah data dari database

UPDATE items SET price = 2500000 WHERE id=1;
